# PythonSelenium

initial version as an example using webdriver & behave for BDD

assuming WSL and Python correctly set up on windows

1. Setup
    * Clone the repo
	* Install the dependencies `pip install -r requirements.txt`
    * create a webdriverdriver dir somewhere, add at least chromedriver (for your chrome version) & add thisdir to your PATH environment variable
    * using PS cd to features and execute behave.exe
