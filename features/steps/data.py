import enum


class user:
    def __init__(self, username, password):
        # code to initialize different sets
        self.username = username
        self.password = password


class urls(enum.Enum):
    # UrlWouter=str("http://trackswouter.westeurope.cloudapp.azure.com/login")
    # UrlDiamanta=str('http://tracksdiamanta.westeurope.cloudapp.azure.com/login')
    url_atlassian = str('https://www.atlassian.com/nl')

    @staticmethod
    def lookup(env):
        if env == "UrlAtlassian":
            return urls.url_atlassian
        # if env == "urlDiamanta":
        #     return urls.UrlDiamanta


class users(enum.Enum):
    # Wouter = user("clark","clark403007!")
    # BadUser = user("drght","cgtdhdy")
    # DiamantaUser =user("","")
    igor_arshinov = user("igor.arshinov@ttl.be", "testingisfun")

    @staticmethod
    def lookup(user):
        if user == "IgorArshinov":
            return users.igor_arshinov
# if user == "Wouter":
#     return users.Wouter
# if user == "BadUser":
#     return users.BadUser
# if user == "DiamantaUser":
#     return users.DiamantaUser
