import time
from functools import partial
from typing import Optional

import selenium
from selenium.common.exceptions import TimeoutException, NoSuchElementException
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.common.by import By
import data
from behave import use_step_matcher, given, when, then
from lib.pages import *

use_step_matcher("parse")


@given(u'we visit "{env}"')
def jira_visit(context, env):
    context.browser.get(data.urls.lookup(env).value)


# @when(u'I submit login form')
# def login_submit(context):
# submit_button = context.browser.find_element(By.CSS_SELECTOR, "button[type='submit']")
# submit_button.click()
# login_submit_button = context.browser.find_element(By.CSS_SELECTOR, "button[id='login-submit']")
# login_submit_button.click()
# login_submit_button = find_element(context, "button[id='login-submit']")
# login_submit_button.click()


def click_element(*values):
    values[0].click()


def send_keys_to_element(*values):
    values[0].send_keys(values[1])


@given(u'I visit "{website}"')
def step_impl(context, website):
    page = AtlassianHomePage(context)
    page.visit(data.urls.lookup(website).value)
    page.login_hyperlink.click()

@when(u'I login using "{name}"')
def enter_jira_credentials(context, name):
    wait = WebDriverWait(context.browser, 15)
    context.sessionUser = data.users.lookup(name)

    wait_find_and_use_element(wait, "a[id='gray_link']", click_element)
    # login_hyperlink = wait_and_find_element(wait, "a[id='gray_link']", click)
    # if not (login_hyperlink is None):
    #     login_hyperlink.click()
    wait_find_and_use_element(wait, "input[id='username']", send_keys_to_element, context.sessionUser.value.username)
    # username_field = wait_and_find_element(wait, "input[id='username']")
    # if not (username_field is None):
    #     username_field.send_keys(context.sessionUser.value.username)
    #
    # find_and_use_element(context, "button[id='login-submit']", click_element)
    login_submit_button = find_element(context, "button[id='login-submit']")
    login_submit_button.click()
    #
    wait_find_and_use_element(wait, "input[id='password']", send_keys_to_element, context.sessionUser.value.password)
    # password_field = wait_and_find_element(wait, "input[id='password']")
    # if not (password_field is None):
    #     password_field.send_keys(context.sessionUser.value.password)
    #
    login_submit_button.click()

    time.sleep(3)


def find_and_use_element(context, element_to_find: str, action, *values):
    try:
        element = context.browser.find_element(By.CSS_SELECTOR, element_to_find)
        action(element, values)
    except NoSuchElementException:
        print("Didn't find the element: " + element_to_find)
    except:
        print("An error happened while trying to find the element: " + element_to_find)


def find_element(context, element_to_find: str):
    try:
        element = context.browser.find_element(By.CSS_SELECTOR, element_to_find)
        return element
    except NoSuchElementException:
        print("Didn't find the element: " + element_to_find)
        return None
    except:
        print("An error happened while trying to find the element: " + element_to_find)
        return None


def wait_find_and_use_element(wait: WebDriverWait, element_to_find: str, action, *values):
    try:
        element = wait.until(
            expected_conditions.visibility_of_element_located((By.CSS_SELECTOR, element_to_find)))
        action(element, values)
    except TimeoutException:
        print("Timed out waiting for the element to load: " + element_to_find)
    except:
        print("An error happened while trying to find the element: " + element_to_find)


# jira_submit_login(context)
# time.sleep(5)


# @when(u'I login using correct credentials')
# def enter_jira_credentials(context):
#     context.sessionUser = context.data.wouter
#     usernameField = context.browser.find_element(By.CSS_SELECTOR, "input[autocomplete='username']")
#
#     usernameField.send_keys(context.data.wouter.username)
#
#     passwordField = context.browser.find_element(By.CSS_SELECTOR, "input[autocomplete='currrent-password']")
#     passwordField.send_keys(context.data.wouter.password)
#     jira_submit_login(context)


@then(u'I should have')
def jira_login_check(context):
    assert context.browser.find_element_by_id("todo_new_action_submit")


@then(u'the logged on user is correct')
def jira_login_check(context):
    context.sessionUser.value.username
    jira_log_off(context)


@when(u' I log off')
def jira_log_off(context):
    time.sleep(1)
