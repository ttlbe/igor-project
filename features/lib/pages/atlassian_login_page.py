__author__ = 'pmacharl'
from selenium.webdriver.common.by import By
from base_page_object import BasePage


class LoginPage(BasePage):
    locator_dictionary = {
        "username_input": (By.CSS_SELECTOR, "input[id='username']"),
        "password_input": (By.CSS_SELECTOR, "input[id='password']"),
        "login_submit_button": (By.CSS_SELECTOR, "button[id='login-submit']")
    }

    def __init__(self, context):
        BasePage.__init__(
            self,
            context.browser,
            base_url='https://id.atlassian.com/login')

    def login(self, username="igor.arshinov@ttl.be", password="testingisfun"):
        self.find_element(*self.locator_dictionary['username_input']).send_keys(username)
        self.find_element(*self.locator_dictionary['login_submit_button']).click()
        self.find_element(*self.locator_dictionary['password_input']).send_keys(password)
        self.find_element(*self.locator_dictionary['login_submit_button']).click()
