from selenium import webdriver


def before_all(context):
	opts = webdriver.ChromeOptions()
	opts.add_argument('no-sandbox')
	context.browser = webdriver.Chrome(chrome_options=opts)


def after_all(context):
	
	context.browser.quit()
	

	
